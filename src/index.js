import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './modules/layout/Layout.jsx';
import "./styles/index.less"

ReactDOM.render(<Layout />, document.getElementById("root"));

