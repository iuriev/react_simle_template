import React from 'react';
import { expect } from 'chai';
import { shallow, configure ,mount} from 'enzyme';
import Layout from '../Layout.jsx';
import Adapter from 'enzyme-adapter-react-16';
import mochaSnapshots from 'mocha-snapshots';
configure({ adapter: new Adapter() });
mochaSnapshots.setup({ sanitizeClassNames: false });

describe('snapshoot test', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(<Layout />)
        expect(wrapper).to.matchSnapshot();

        // // Strings
        // expect('you can match strings').to.matchSnapshot();
        //
        // // Numbers
        // expect(123).to.matchSnapshot();
        //
        // // Or any object
        // expect({ a: 1, b: { c: 1 } }).to.matchSnapshot();

    });
});